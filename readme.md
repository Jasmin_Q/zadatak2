Installation
Please check the official laravel installation guide for server requirements before you start. Official Documentation

Clone the repository

git clone git@bitbucket.org:Jasmin_Q/zadatak2.git
Switch to the repo folder

cd zadatak2
Install all the dependencies using composer

composer install
Copy the example env file and make the required configuration changes in the .env file

cp .env.example .env
Generate a new application key

php artisan key:generate

Run the database migrations (Set the database connection in .env before migrating)

php artisan migrate
Start the local development server

php artisan serve
You can now access the server at http://localhost:8000

To test customers CRUD http://localhost:8000/customers

To test employees CRUD http://localhost:8000/employees


