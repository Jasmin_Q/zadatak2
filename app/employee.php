<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'company',
        'last_name',
        'first_name',
        'email_address',
        'job_title',
        'business_phone',
        'home_phone',
        'mobile_phone',
        'fax_number',
        'address',
        'city'

    ];

    public function privileges()
    {
        return $this->belongsToMany('App\Privilege');
    }
}
